# Simple Recipe

## A simple record plugin, to show the records in list view and record on click shows the detail view of the record

### Getting started

* Install extension
* Include static TypoScript

### Plugin usage

* Create page that'll contain plugin
* Add new content element, go to "Plugins" tab and choose "Recipe"
    * Add the storage to store the Recipes
    * Add the Recipes in the storage